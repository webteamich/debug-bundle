<?php

namespace Truelab\Bundle\DebugBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TemplateController extends Controller implements TemplateControllerInterface
{
    /**
     * @Route("/template/{bundleName}/{templateName}")
     */
    public function indexAction($bundleName, $templateName)
    {
        $content = $this->renderView($this->getTemplate($bundleName, $templateName));
        return new Response($content);
    }

    /**
     * @param $bundleName
     * @param $templateName
     * @return string
     */
    protected function getTemplate($bundleName, $templateName)
    {
        return $bundleName.':Template:'.$templateName.'.html.twig';
    }
}