<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy;

abstract class AbstractStrategy implements StrategyInterface
{
    protected $_config;
    protected $message = '';

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_config = $config;
    }


    public function isForbidden()
    {
        return $this->isAllowed() === false;
    }

    public function getForbiddenMessage()
    {
        $base = '[APP_DEV_FIREWALL]['.strtoupper($this->getName()).'] <br><br>You are not allowed to access this file. Check app_dev.php for more information.<br>';
        $base .= $this->message;
        return $base;
    }
}