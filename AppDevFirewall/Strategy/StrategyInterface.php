<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy;

interface StrategyInterface
{

    /**
     * Current ... is forbidden?
     *
     * @return bool
     */
    public function isForbidden();

    /**
     * Current ... is allowed?
     *
     * @return bool
     */
    public function isAllowed();

    /**
     * Get forbidden message
     *
     * @return string
     */
    public function getForbiddenMessage();

    /**
     * Determine if strategy is active
     *
     * @param  array $config
     * @return bool
     */
    public static function isEnabled($config);


    /**
     * Get strategy alias name
     *
     * @return string
     */
    public static function getName();
}