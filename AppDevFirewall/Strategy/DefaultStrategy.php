<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy;

use Sonata\MediaBundle\Tests\Provider\YoutubeProviderTest;

class DefaultStrategy extends AbstractStrategy
{
    protected $message = 'Your ip doesn\'t appear on the basic list.';
    public static $basicIps = array('127.0.0.1', 'fe80::1', '::1');

    public function isAllowed()
    {
        return in_array(@$_SERVER['REMOTE_ADDR'], self::$basicIps);
    }


    public static function isEnabled($config)
    {
        if(isset($config[self::getName()]) && $config[self::getName()] === false) {
            return false;
        }
        
        return empty($config) || array_key_exists(self::getName(), $config);
    }

    public static function getName()
    {
        return 'default';
    }
}