<?php

namespace Truelab\Bundle\DebugBundle\DataCollector;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class TemplatesPreviewsDataCollector extends DataCollector
{

    protected $bundlesMap = [];
    protected $count = 0;
    protected $route;

    /**
     * @param string $route
     * @param array $bundlesConfigs
     * @param \AppKernel $kernel
     */
    public function __construct($route, $bundlesConfigs, $kernel)
    {
        $this->route = $route;

        /**
         * @var \Symfony\Component\HttpKernel\Bundle\Bundle[]
         */
        $bundles = [];
        foreach($bundlesConfigs as $bundleConfig) {
            $bundles[] = $kernel->getBundle($bundleConfig['name']);
        }


        /**
         * @var $bundles \Symfony\Component\HttpKernel\Bundle\Bundle[]
         */
        foreach($bundles as $bundle) {

            $bundlePath = $bundle->getPath().'/Resources/views/Template/';
            $finder     = new Finder();

            $finder
                ->files()
                ->name('*.html.twig')
                ->in($bundlePath)
                ->sortByName();

            $templates  = [];
            foreach ($finder as $file) {
                $templates[] = str_replace('.html.twig','', $file->getFilename());
                $this->count = $this->count + 1;
            }

            $this->bundlesMap[$bundle->getName()] = [
                'name'      => $bundle->getName(),
                'path'      => $bundlePath,
                'templates' => $templates,
                'count'     => sizeof($templates)
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'route'      => $this->route,
            'bundlesMap' => $this->bundlesMap,
            'count'      => $this->count
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'templates_previews';
    }

    /**
     * Active bundles map
     * @return mixed
     */
    public function getBundlesMap()
    {
        return $this->data['bundlesMap'];
    }

    /**
     * Total templates number
     * @return int
     */
    public function getCount()
    {
        return $this->data['count'];
    }

    /**
     * Controller Route
     * @return string
     */
    public function getRoute()
    {
        return $this->data['route'];
    }
}