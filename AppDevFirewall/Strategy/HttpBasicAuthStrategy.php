<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy;

class HttpBasicAuthStrategy extends AbstractStrategy
{

    protected $configuration;
    protected $message = 'Wrong credentials. Refresh the page to try again.';
    protected $users   = array();

    public function __construct($config)
    {
        parent::__construct($config);

        $this->configuration = $this->_config[self::getName()];

        if($this->configuration != false && !isset($this->configuration['users'])) {
            throw new \Exception('You have to provide users in the parameters file to be able to use AppDevFirewall HttpBasicAuthStrategy');
        }else{
            $this->users = $this->configuration['users'];
        }

    }

    public function isAllowed()
    {
        if($this->configuration === false) {
            return true;
        }

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            $this->authenticate();

        } else {

            if($this->checkUserAndPassword()) {
                return true;
            }else{
                $this->authenticate();
            }
        }
    }


    public static function isEnabled($config)
    {
        return is_array($config) && array_key_exists(self::getName(), $config);
    }

    protected function authenticate()
    {
        header('WWW-Authenticate: Basic realm="[AppDevFirewall] you have to provide a correct username and a password to access te app_dev.php front-controller"');
        header('HTTP/1.0 401 Unauthorized');
        exit($this->getForbiddenMessage());
    }

    protected function checkUserAndPassword()
    {
        foreach($this->users as $user) {
            if(isset($user['user'])
                && isset($user['password'])
                && ($user['user']) === $_SERVER['PHP_AUTH_USER']
                && ($user['password']) === $_SERVER['PHP_AUTH_PW']) {

                return true;
            }
        }

        return false;
    }

    public static function getName()
    {
        return 'http_basic';
    }

}