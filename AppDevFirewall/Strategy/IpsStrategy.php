<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy;

class IpsStrategy extends AbstractStrategy
{
    protected $message = 'Your ip doesn\'t appear on the list.';
    protected $allowIps;

    public function __construct($config)
    {
        parent::__construct($config);

        $this->allowIps = $this->_config[self::getName()];
    }

    public function isAllowed()
    {
        $ips = [];

        if($this->allowIps === false) {
            $ips = [];
        }

        if($this->allowIps === true || $this->allowIps === NULL) {
            $ips = DefaultStrategy::$basicIps;
        }

        if(is_array($this->allowIps)) {
            $ips = array_merge(DefaultStrategy::$basicIps, $this->allowIps);
        }

        return in_array(@$_SERVER['REMOTE_ADDR'], $ips);
    }

    public static function isEnabled($config)
    {
        return is_array($config) && array_key_exists(self::getName(), $config);
    }

    public static function getName()
    {
        return 'ips';
    }
}