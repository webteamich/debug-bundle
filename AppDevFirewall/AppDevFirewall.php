<?php

namespace Truelab\Bundle\DebugBundle\AppDevFirewall;

/**
 * Class AppDevFirewall
 * @package Truelab\Bundle\DebugBundle\AppDevFirewall
 *
 * @description
 * This class is used before symfony kernel is loaded to
 * forbid/allow access of the app_dev.php front-controller (@see web/app_dev.php),
 * replacing the simple check that symfony standard edition provides.
 *
 * !!! we don't have symfony sugar here, because kernel is not loaded yet (no container, no DI, no party).
 *
 * @example
 * <?php
 * // web/app_dev.php
 *
 *
 * $appDevFirewall = new AppDevFirewall([
 *     'app_dev_firewall' => [
 *         'default' => true
 *     ]
 * ]);
 *
 * if ($appDevFirewall->isForbidden()) {
 *     header('HTTP/1.0 403 Forbidden');
 *     echo($appDevFirewall->getForbiddenMessage());
 *     exit;
 * }
 *
 *
 *
 * //...
 *
 * @example yml config
 *
 * app_dev_firewall:
 *     default: true  # default symfony check, forbid outside localhost
 *     ips : ['198.178.55.70','198.178.55.80'] # allow this ips and localhost
 *     http_basic: # use http basic auth
 *       users:
 *         - {user: foo, password: bar}
 *         - {user: bar, password: foo}
 *
 * @example php config
 *
 * array(
 *     'app_dev_firewall' => array(
 *          'default' => true,
 *          'ips' => array('198.178.55.70','198.178.55.80'),
 *          'http_basic' => array(
 *              'users' => array(
 *                  array('user' => 'foo', 'password' => 'bar'),
 *                  array('user' => 'bar', 'password' => 'foo')
 *              )
 *          )
 *      )
 * );
 *
 */
class AppDevFirewall {

    protected $config;
    protected $parameters;

    protected $forbiddenMessage = '';
    protected $strategies = [];
    protected $strategyClasses = [
        'Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy\DefaultStrategy',
        'Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy\IpsStrategy',
        'Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy\HttpBasicAuthStrategy'
    ];


    /**
     * @param array $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
        $this->config     = $this->getConfig();
        $this->strategies = $this->buildStrategiesStack();
    }

    /**
     * @return bool
     */
    public function isForbidden()
    {
        return $this->isAllowed() === false;
    }


    /**
     * @return bool
     */
    protected function isAllowed()
    {
        if(isset($_SERVER['HTTP_CLIENT_IP'])
            || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
            || php_sapi_name() === 'cli-server'
        ){
            return false;
        }

        /**
         * @var $strategy Strategy\StrategyInterface
         */
        foreach($this->strategies as $strategy) {
            if($strategy->isForbidden()) {
               $this->forbiddenMessage = $strategy->getForbiddenMessage();
               return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getForbiddenMessage()
    {
        return $this->forbiddenMessage;
    }

    /**
     * Build the strategies stack
     *
     * @return array Truelab\Bundle\DebugBundle\AppDevFirewall\Strategy\StrategyInterface[]
     */
    protected function buildStrategiesStack()
    {
        // no strategies to instantiate return empty array, no stack
        if($this->config === false || $this->config === NULL) {
            return $this->strategies;
        }

        /**
         * @var $strategyClass Strategy\StrategyInterface
         */
        foreach($this->strategyClasses as $strategyClass) {

            if($strategyClass::isEnabled($this->config)) {
                $this->strategies[] = new $strategyClass($this->config);
            }
        }

        return $this->strategies;
    }

    /**
     * @return array
     */
    protected function getConfig()
    {
        $config  = [];

        if( isset($this->parameters[self::getName()]) ) {
            $config = $this->parameters[self::getName()];
        }

        return $config;
    }

    /**
     * @return string
     */
    protected static function getName()
    {
        return 'app_dev_firewall';
    }
}