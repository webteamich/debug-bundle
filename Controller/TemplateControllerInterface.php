<?php

namespace Truelab\Bundle\DebugBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

interface TemplateControllerInterface
{
    /**
     * @param string $bundleName
     * @param string $templateName
     * @return Response
     */
    public function indexAction($bundleName, $templateName);
}